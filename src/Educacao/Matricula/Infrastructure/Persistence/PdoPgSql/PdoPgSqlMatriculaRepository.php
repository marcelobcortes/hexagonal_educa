<?php

namespace Acruxx\Educacao\Matricula\Infrastructure\Persistence\PdoPgSql;

use Acruxx\Educacao\Matricula\Domain\Repository\MatriculaRepository;
use Acruxx\Educacao\Matricula\Domain\Entity\Matricula;
use Acruxx\Educacao\Matricula\Domain\ValueObject\IdMatricula;

class PdoPgSqlMatriculaRepository implements MatriculaRepository
{
    
    /** \PDO */
    private $pdo;

    public function __construct(\PDO $pdo)
    {
        $this->pdo = $pdo;
    }

    public function store(Matricula $matricula) : void {

        if ($this->findById($matricula->getId())) {
            $stm = $this->pdo->prepare('
                UPDATE
                    matriculas
                SET
                    id_aluno = :id_aluno,
                    id_calsse = :id_classe,
                    status = :status,
                    data_matricula = :data_matricula,

                WHERE
                    id=:id
            ');
        } else {
            $stm = $this->pdo->prepare('
                INSERT INTO
                    matriculas

                VALUES (
                    :id,
                    :id_aluno,
                    :id_classe,
                    :status,
                    :data_matricula
                    )
            ');
        }

        $id = $matricula->getId()->toString();
        $idAluno = $matricula->getAluno()->getId()->toString();
        $idClasse = $matricula->getClasse()->getId()->toString();
        $status = $matricula->getStatus()->toString();
        $dataMatricula = $matricula->getData()->toString();

        $stm->bindParam(':id', $id, \PDO::PARAM_STR);
        $stm->bindParam(':id_aluno', $idAluno, \PDO::PARAM_STR);
        $stm->bindParam(':id_classe', $idClasse, \PDO::PARAM_STR);
        $stm->bindParam(':status', $status, \PDO::PARAM_STR);
        $stm->bindParam(':data_matricula', $dataMatricula);

        if (!$stm->execute()){
            \error_log($this->pdo->errorInfo());
            throw new \RuntimeException('Ocorreu um problema ao inserir o aluno');
        }
    }

    public function findById(IdMatricula $id) : ?Matricula
    {
        $id = $id->toString();

        $query = 'SELECT * from matriculas WHERE id=:id';
        $stm = $this->pdo->prepare($query);
        $stm->bindParam(':id', $id, \PDO::PARAM_STR);
        $stm->execute();

        $result = $stm->fetch(\PDO::FETCH_ASSOC);

        return is_array($result) ? $this->createMatriculaFromArray($result) : null;
    }

    public function findAll() : array
    {
        $stm = $this->pdo->prepare('
            SELECT
                m.id,
                m.id_aluno,
                a.nome                    AS nome_aluno,
                m.id_classe,
                \'Nome da Classe Fake\'   AS nome_classe,
                m.status,
                m.data                    AS data_matricula

            FROM
                matriculas m
                
                INNER JOIN alunos a
                    ON a.id = m.id_aluno
        ');

        $stm->execute();

        $arrMatriculas = $stm->fetchAll(\PDO::FETCH_ASSOC);

        return \array_map(function(array $arrMatricula) {
            return $this->createMatriculaFromArray($arrMatricula);
        }, $arrMatriculas);
    }

    private function createMatriculaFromArray(array $result) : Matricula
    {
        return Matricula::populate($result);
    }

}