<?php

namespace Acruxx\Educacao\Matricula\Application\Ui;

use Slim\Http\Response;
use Slim\Http\Request;
use Psr\Container\ContainerInterface;
use Acruxx\Educacao\Matricula\Domain\Repository\AlunoRepository;
use Acruxx\Educacao\Matricula\Domain\Repository\ClasseRepository;
use Acruxx\Educacao\Matricula\Domain\Dto\MatriculaAlunoDto;
use Acruxx\Educacao\Matricula\Domain\Service\NovaMatriculaAluno;
use Acruxx\Educacao\Matricula\Domain\Repository\MatriculaRepository;

final class MatriculaController
{
    /** @var ContainerInterface */
    private $container;

    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    public function show(Request $req, Response $res) : Response
    {
        $alunoRepository = $this->container->get(AlunoRepository::class);
        $classeRepository = $this->container->get(ClasseRepository::class);
        $matriculasRepository = $this->container->get(MatriculaRepository::class);

        $alunos = $alunoRepository->findAll();
        $classes = $classeRepository->findAll();
        $matriculas = $matriculasRepository->findAll();

        $flashMessages = $this->container->flash->getMessages();

        return $this->container->view->render($res, 'matricula.html.twig',[
            'alunos' => $alunos,
            'classes' => $classes,
            'formMessage' => $flashMessages['form_message'] ?? null,
            'matriculas' => $matriculas
        ]);
    }

    public function insert(Request $req, Response $res) : Response
    {
        $matriculaAlunoDto = MatriculaAlunoDto::fromArray($req->getParams());
        
        $this->container->get(NovaMatriculaAluno::class)->matricula($matriculaAlunoDto);
        
        $this->container->flash->addMessage('form_message', 'Matricula criada com sucesso.');

        return $res->withRedirect('/matricula/nova');
    }
}
