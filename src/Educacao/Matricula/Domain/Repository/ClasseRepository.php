<?php

namespace Acruxx\Educacao\Matricula\Domain\Repository;

use Acruxx\Educacao\Matricula\Domain\ValueObject\IdClasse;
use Acruxx\Educacao\Matricula\Domain\Entity\Classe;

interface ClasseRepository {

    public function getById(IdClasse $id) : Classe;
    /** @return Classe[] */
    public function findAll() : array;
}
