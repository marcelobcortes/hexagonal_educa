<?php

namespace Acruxx\Educacao\Matricula\Domain\Repository;

use Acruxx\Educacao\Matricula\Domain\ValueObject\IdALuno;
use Acruxx\Educacao\Matricula\Domain\Entity\Aluno;

interface AlunoRepository {

    public function getById(IdAluno $id) : Aluno;

    /** @return Aluno[] */
    public function findAll() : array;
}
