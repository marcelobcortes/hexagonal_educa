<?php

namespace Acruxx\Educacao\Aluno\Domain\Listener;

use Acruxx\Educacao\Aluno\Domain\Event\AlunoFoiCadastrado;
use Acruxx\Educacao\Aluno\Domain\Event\AlunoFoiArquivado;

class NotificaAlunoBengaMoleListener implements Listener
{
    public function handle($event) : void
    {
        if ($event instanceof AlunoFoiCadastrado) {
            \error_log(
                <<<HTML
                O aluno {$event->getNome()->toString()} tem a benga mole
                HTML
            );
            return;
        }
        if ($event instanceof AlunoFoiArquivado) {
            \error_log("O aluno ID {$event->getId()->toString()} foi arquivado por ter benga mole");
            return;
        }
        
        throw new \RuntimeException('Fudeu, corra pras colinas');
    }

}