<?php

namespace Acruxx\Educacao\Aluno\Domain\Listener;

use Acruxx\Educacao\Aluno\Domain\Event\AlunoFoiCadastrado;
use Acruxx\Educacao\Aluno\Domain\Event\AlunoFoiArquivado;

class NotificaMaeDoAlunoListener implements Listener
{
    public function handle($event) : void
    {
        if ($event instanceof AlunoFoiCadastrado) {
            \error_log(
                <<<HTML
                Notificando a m�e do aluno {$event->getNome()->toString()} do ID {$event->getId()->toString()}
                HTML
            );
            return;
        } else if ($event instanceof AlunoFoiArquivado) {
            \error_log(
                <<<HTML
                Notifica a mae do aluno ID {$event->getId()->toString()} foi arquivado
                HTML
            );
            return;
        }

        throw new \RuntimeException('Sei la');
    }
}