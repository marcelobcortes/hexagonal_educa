<?php

namespace Acruxx\Educacao\Aluno\Application\Rest;

use Slim\Http\Request;
use Slim\Http\Response;
use Psr\Container\ContainerInterface;

use \Acruxx\Educacao\Aluno\Domain\Repository\AlunoRepository;
use Acruxx\Educacao\Aluno\Domain\ValueObject\IdAluno;

final class GetAlunoAction extends AbstractAction
{

    public function handle(Request $req, Response $res, array $args = []) : Response
    {
        $id = $args['id'] ?? '';

        $IdAluno = IdAluno::fromString($id);

        $aluno = $this->container->get(AlunoRepository::class)->getById($IdAluno);
        
        return $res->withStatus(200)->withJson([
            'id' => $aluno->getId()->toString(),
            'nome' => $aluno->getNome()->toString(),
            'arquivado' => $aluno->arquivado()
        ]);
    }

}