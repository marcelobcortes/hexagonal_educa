<?php

use \Acruxx\Educacao\Aluno\Domain\Service\CadastraAluno;
use \Acruxx\Educacao\Aluno\Domain\Repository\AlunoRepository;
use \Psr\Container\ContainerInterface;
use Acruxx\Educacao\Aluno\Domain\Service\ArquivaAluno;
use Acruxx\Educacao\Aluno\Infrastructure\Persistence\PdoPgSql\PdoPgSqlRepository;
use Acruxx\Educacao\Aluno\Domain\Event\Dispatcher;
use Acruxx\Educacao\Aluno\Infrastructure\Event\AcruxxDispatcher;
use Acruxx\Educacao\Aluno\Domain\Event\AlunoFoiCadastrado;
use Acruxx\Educacao\Aluno\Domain\Listener\NotificaMaeDoAlunoListener;
use Acruxx\Educacao\Aluno\Domain\Listener\NotificaAlunoBengaMoleListener;
use Acruxx\Educacao\Aluno\Domain\Event\AlunoFoiArquivado;
use Acruxx\Educacao\Matricula;

$config['settings'] = [
    "displayErrorDetails" => true
];

$container = new \Slim\Container($config);


$container['view'] = function ($container) {
    $view = new \Slim\Views\Twig(__DIR__ . '/../res/templates', [
        'cache' => false //__DIR__ . '/../data/cache'
    ]);
  
    // Instantiate and add Slim specific extension
    $router = $container->get('router');
    $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
    $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));
  
    return $view;
};

session_start();

$container['flash'] = function () {
    return new \Slim\Flash\Messages();
};

$container[ArquivaAluno::class] = static function (ContainerInterface $container) {
    $alunoRepository = $container->get(AlunoRepository::class);
    $dispatcher = $container->get(Dispatcher::class);

    return new ArquivaAluno($alunoRepository, $dispatcher);
};

$container[CadastraAluno::class] = static function (ContainerInterface $container) {
    $alunoRepository = $container->get(AlunoRepository::class);
    $dispatcher = $container->get(Dispatcher::class);

    return new CadastraAluno($alunoRepository, $dispatcher);
};

$container[AlunoRepository::class] = static function (ContainerInterface $container) {
    /* $storageDir  = __DIR__ . '/../data/storage/';
    $storageFile = 'alunos.json';

    return new JsonAlunoRepository($storageDir, $storageFile); */

    return new PdoPgSqlRepository($container->get('pdo-connection'));
};

$container['pdo-connection'] = static function () {
    $pdo = new \PDO(
        'pgsql:host=postgres;dbname=php;user=root;password=123'
    );
    $pdo->setAttribute(\PDO::ATTR_ERRMODE, \PDO::ERRMODE_EXCEPTION);

    return $pdo;
};

$container[Dispatcher::class] = static function () {
    $dispatcher = new AcruxxDispatcher();

    $dispatcher->attach(AlunoFoiArquivado::class, new NotificaMaeDoAlunoListener());
    $dispatcher->attach(AlunoFoiArquivado::class, new NotificaAlunoBengaMoleListener());

    $dispatcher->attach(AlunoFoiCadastrado::class, new NotificaMaeDoAlunoListener());
    $dispatcher->attach(AlunoFoiCadastrado::class, new NotificaAlunoBengaMoleListener());

    return $dispatcher;
};

/**
 * Contexto Matrícula
 */
$container[Matricula\Domain\Repository\AlunoRepository::class] = static function(ContainerInterface $container) {
    return new Matricula\Infrastructure\Persistence\Component\ComponentAlunoRepository(
        $container->get(AlunoRepository::class));
};

$container[Matricula\Domain\Repository\ClasseRepository::class] = static function(ContainerInterface $container) {
    return new Matricula\Infrastructure\Persistence\Fake\FakeClasseRepository();
};

$container[Matricula\Domain\Repository\MatriculaRepository::class] = static function(ContainerInterface $container) {
    $pdo = $container->get('pdo-connection');
    return new Matricula\Infrastructure\Persistence\PdoPgSql\PdoPgSqlMatriculaRepository($pdo);
};

$container[Matricula\Domain\Service\NovaMatriculaAluno::class] = static function(ContainerInterface $container) {
    $matriculaRepository = $container->get(Matricula\Domain\Repository\MatriculaRepository::class);
    $alunoRepository = $container->get(Matricula\Domain\Repository\AlunoRepository::class);
    $classeRepository = $container->get(Matricula\Domain\Repository\ClasseRepository::class);
    
    return new Matricula\Domain\Service\NovaMatriculaAluno(
        $matriculaRepository,
        $alunoRepository,
        $classeRepository
    );
};

return $container;