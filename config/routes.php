<?php

use Acruxx\Educacao\Aluno\Application\Rest;
use Acruxx\Educacao\Matricula\Application\Ui\MatriculaController;

$app->post('/rest/v3/aluno', new Rest\CadastraAlunoAction($container));

$app->put('/rest/v3/arquiva', new Rest\ArquivaAlunoAction($container));

$app->get('/rest/v3/aluno', new Rest\GetAlunosAction($container));

$app->get('/rest/v3/aluno/{id}', new Rest\GetAlunoAction($container));


// UI
$app->get('/matricula/nova', MatriculaController::class . ':show');

$app->post('/matricula/insert', MatriculaController::class . ':insert');