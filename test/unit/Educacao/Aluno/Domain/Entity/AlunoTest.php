<?php

namespace EducacaoUnitTest\Aluno\Domain\Entity;

use PHPUnit\Framework\TestCase;
use Acruxx\Educacao\Aluno\Domain\Entity\Aluno;
use Acruxx\Educacao\Aluno\Domain\Dto\CadastraAlunoDto;
use Acruxx\Educacao\Aluno\Domain\Event\AlunoFoiCadastrado;
use Acruxx\Educacao\Aluno\Domain\ValueObject\Nome;
use Acruxx\Educacao\Aluno\Domain\ValueObject\NomeMae;
use Acruxx\Educacao\Aluno\Domain\ValueObject\RA;
use Acruxx\Educacao\Aluno\Domain\Event\AlunoFoiArquivado;

class AlunoTest extends TestCase
{
    /** @var MockObject */
    private $cadastraAlunoDto;

    /** array */
    private $paramsTest;

    public function setUp() : void
    {
        $this->paramsTest = [
            "nome" => Nome::fromString("Kiko"),
            "nomeMae" => NomeMae::fromString("Florinda"),
            "ra" => RA::fromString("12345678901")
        ];

        $this->cadastraAlunoDto = $this
                                ->getMockBuilder(CadastraAlunoDto::class)
                                ->disableOriginalConstructor()
                                ->getMock();
        $this->cadastraAlunoDto
                        ->expects($this->once())
                        ->method('getNome')
                        ->willReturn($this->paramsTest['nome']);
        $this->cadastraAlunoDto
                        ->expects($this->once())
                        ->method('getRa')
                        ->willReturn($this->paramsTest['ra']);
        $this->cadastraAlunoDto
                        ->expects($this->once())
                        ->method('getNomeMae')
                        ->willReturn($this->paramsTest['nomeMae']);
    }

    /** @test */
    public function novoAlunoDeveFuncionarQuandoForDto() : void
    {
        $aluno = Aluno::novoAluno($this->cadastraAlunoDto);

        $this->assertNotNull($aluno->getId());
        $this->assertEquals($this->paramsTest['nome'], $aluno->getNome());
        $this->assertEquals($this->paramsTest['nomeMae'], $aluno->getNomeMae());
        $this->assertEquals($this->paramsTest['ra'], $aluno->getRa());

        $events = $aluno->releaseEvents();

        $this->assertTrue(count($events) > 0, 'Aluno n�o emitiu evento de novo cadastro.');
        $this->assertInstanceOf(AlunoFoiCadastrado::class, $events[0]);
    }
    
    /** @test */
    public function novoAlunoDeveFuncionarQuandoForArray() : void
    {
        $arrParamsTest = [
            'id' => 'IdAluno333',
            'arquivado' => true,
            'data_arquivado' => '2019-05-29'
        ];

        $aluno = Aluno::novoAluno($this->cadastraAlunoDto, $arrParamsTest);

        $this->assertEquals($arrParamsTest['id'], $aluno->getId()->toString());
        $this->assertEquals($arrParamsTest['arquivado'], $aluno->arquivado());
        $this->assertEquals($arrParamsTest['data_arquivado'], $aluno->getDataArquivado()->format('Y-m-d'));

        $this->assertCount(0, $aluno->releaseEvents(), 'Aluno n�o pode emitir evento quando criado por array');
    }
    
    /** @test 
    */
    public function arquivaDeveFuncionar() : void
    {
        $aluno = Aluno::novoAluno($this->cadastraAlunoDto);

        $this->assertFalse($aluno->arquivado());
        $this->assertNull($aluno->getDataArquivado());

        $aluno->arquiva();

        $this->assertTrue($aluno->arquivado());
        $this->assertNotNull($aluno->getDataArquivado());

        $events = $aluno->releaseEvents();
        $this->assertTrue(count($events) > 0, 'Aluno n�o emitiu evento de arquivado');
        $this->assertInstanceOf(AlunoFoiArquivado::class, array_pop($events));
    }
}